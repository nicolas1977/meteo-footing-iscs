package readers;
import general.ElementArray;
import general.MultiValue;

import java.io.*;
import java.lang.annotation.ElementType;
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;
import java.util.SimpleTimeZone;
import java.util.regex.Pattern;

public class ReadersFiles {
    int i;
    String fdir;
    private int splitingFunction[];
    public static int A[];
    int[][] calculArray;
    /*not need to instancie the class in internal , but for clarty to make this*/
    //public ReadersFiles rf;
    /*array dynamic variables*/
    public int leightF;
    public RandomAccessFile frandom;
    public ElementArray eObj;


    static ArrayList<ElementArray> aList = new ArrayList<ElementArray>();

    //calculArray[][][] el = new calculArray[10][10][10];

    //String f="weather.dat";
    public ArrayList<ElementArray> Lecteur(String f, int choix) {
        try {
            // pour etre sur que la absolute path soit la bonne de f
            fdir = new File(f).getAbsolutePath();
            // System.out.println(fdir);

            //rf = new ReadersFiles();

            /*passing a max vlaue of records , one once calling methods
            leightF=leightRecordMax(fdir);*/


            // instance of variable objet in fr to write/read file.DAT generally

            frandom = new RandomAccessFile(fdir,"rw");
            // in general not need to indicate de Abs Path fdir : BY DEFAULT the directory is cela to the name of project
            // cad : /Users/easycash/GitRepositories/meteo-footing-iscs/weather.dat
            // donc seul fr = new FileReader(f);
            // pointe au meme objet reference ;

            // System.out.println("the fr objet pointer reference is : "+frandom);

            /*scoped variables*/
            boolean eof = true;
            String line="";
            int bytes=0;
            frandom.seek(0);
            int i=0;

            do {
                try {
                    bytes=frandom.read();
                    line=frandom.readLine();
                    // System.out.println(line);
                    // System.out.println(bytes);
                    /*it possible, but not need to calling Two time the same function
                    ivalue = rf.splitingFunction("error")[0];
                    */
                    // System.out.println(aList);
                    if (inNumerical(firstChar(line))) {
                        //aList=ReadersFiles.cloneArrayList(aList);
                        aList.add(this.assignElementArray(splitingFunction(line,choix)));

                        // System.out.println(this.AssSplit(line, aList, i));
                    }
                    i=i+1;
                }catch (EOFException ex){
                    break;
                }catch(IOException e){
                    e.printStackTrace();
                }
            } while (frandom.read() != -1);

            /*return for numerical calcul*/
            return aList;

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            System.out.println("---------------------------------------------------------");
        }
        return null;
    }

    /*method to assignement the fields to instanced objet el type ElementArray*/
    public ElementArray assignElementArray (MultiValue splitingFunction){

        eObj = new ElementArray();
        /*accessing variable derived type within Array multidimentional et instanced its*/
        eObj.cArray = new int[10][10];

        /*cases of differents choix programs*/
        if(splitingFunction.A[4]==1) {
            /*assignement to numerical of record value*/
            eObj.cArray[0][0] = splitingFunction.A[0];
            /*assignement to numerical fields  difference  max - min temp value*/
            eObj.cArray[1][0] = splitingFunction.A[1] - splitingFunction.A[2];
        }else if(splitingFunction.A[4]==2) {
            /*assignement to numerical of record value*/
            eObj.denomField = splitingFunction.B[0];
            /*assignement to numerical fields  difference  max - min goals value*/
            eObj.cArray[1][0] = Math.abs(splitingFunction.A[0] - splitingFunction.A[1]);
        }
        /*copy for calcule numerique in methode calcule max from ArrayList*/
        eObj.cArray[0][1]=eObj.cArray[0][0];
        eObj.cArray[1][1]=eObj.cArray[1][0];
        /* System.out.println("record for foot:_"+eObj.denomField);
        System.out.println("record for match eObj.cArray[0][1]:_"+eObj.cArray[0][1]+"\n"+"record .cArray[1][1]_" + eObj.cArray[1][1]);*/

        return eObj;
    }


    public static MultiValue splitingFunction(String dummy, int choix){
        // char ch = ' ';
        /*trim de "*" bit not concerned*/
        dummy=dummy.replaceAll("[*.]", " ");
        //dummy=dummy.replace(".", " ");
        /*it's some thing from linux bash " +" or RegEx, that substitue the all + chars after the first empty  " +" */
        dummy=dummy.trim().replaceAll(" +", ";");
        //int A[];
        //String B[];
        MultiValue mV=new MultiValue(new int[10],new String[10]);
        /*initializing derived Type class within multidimentional Arrays*/
        /*splitting values by comma ";", for only 5 element in temporary Array Split*/
        //case weather
        if(choix==1) {
            mV.A[0] = Integer.parseInt(String.valueOf(dummy.split(";", 5)[0]));
            mV.A[1] = Integer.parseInt(String.valueOf(dummy.split(";", 5)[1]));
            mV.A[2] = Integer.parseInt(String.valueOf(dummy.split(";", 5)[2]));
            /*mV.A[3] = Integer.parseInt(String.valueOf(dummy.split(";", 5)[3]));*/
        //case foot
        }else if (choix==2){
            mV.B[0] = dummy.split(";", 10)[1];
            mV.A[0] = Integer.parseInt(String.valueOf(dummy.split(";", 10)[6]));
            mV.A[1] = Integer.parseInt(String.valueOf(dummy.split(";", 10)[8]));
        }
        /*reserved element int value for choix of program*/
        mV.A[4]=choix;

        return mV;
    }

    /*method to verify if the firts char of string is a Numerical*/
    public boolean inNumerical(String chars){
        String regex="[0-9]+[\\.]?[0-9]*";
        /*true if is numerical with 0-9*/
        return Pattern.matches(regex, chars);
    }

    /*not needed methods
    public boolean inSpecial(String chars){
        String regex="[-.*]";
        *//*true if is special with -*//*
        return Pattern.matches(regex, chars);
    }*/

    public String firstChar(String charsFirst){
        charsFirst = charsFirst.trim().replaceAll(" +", ";");
        charsFirst = charsFirst.trim().substring(0,1).trim();
        // System.out.println(charsFirst);
        return charsFirst;
    }
}
