package calculeNumerical;

import java.awt.*;

public class Permu {

    private static int n;
    static int[] A = {1, 2, 3, 4};
    private int i = 0;

    private void print() {
        System.out.println(String.join("-", Integer.toString(A[0]),
               Integer.toString(A[1]),Integer.toString(A[2]), Integer.toString(A[3])));
        }


    private void permute(int i, int k) {
        int temp = A[i];
        A[i] = A[k-1];
        A[k-1] = temp;
    }
/*
    public int[] A(int n){
        int i = 0;
        A = new int[n];
        do {
            A[i] = i;
            i = i + 1;
        } while (i < n);
        return A;
    }
*/

    public void listePermutations(Permu per, int n) {
        try {

            // System.out.println("The number choix for permutation sans rip is :\n" + n);
            if (n == 1) {
                per.print();
            } else {
                per.listePermutations(per, n - 1);
                i=0;
                do {
                    if (n % 2 == 1) {
                        per.permute(0, n);
                    } else {
                        per.permute(i, n);
                    }
                    per.listePermutations(per, n - 1);
                    i = i + 1;
                } while (i < n-1);
            }
        } catch (Exception e) {
            System.out.println("error detected" + e);

        }
    }
}
