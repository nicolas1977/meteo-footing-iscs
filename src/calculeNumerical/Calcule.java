package calculeNumerical;

import general.ElementArray;
import general.MultiValue;

import java.util.ArrayList;

public class Calcule {

    public int C[];
    /*fondamental numerical calcul method to output a max from an Array */
    public MultiValue calcule(ArrayList<ElementArray> aListDef,int choix) {

        MultiValue mValDef = new MultiValue(new int[10], new String[10]);
        int i = 0;
        int j = 0;
        int k = 0;
        // System.out.println(aListDef);
        try {
            C=new int[aListDef.size()];
            do {
                /*with getter in Element Array Class*/
                /*only with setters outside package objet you can access the fields class*/
                /*System.out.println("fields denomField :_" + aListDef.get(i).getDenomField());
                System.out.println("fields valueField :_" + aListDef.get(i).getcArray()[0][1]);*/
                j=1+i;
                do {
                    // if(i>=j){
                    //  j = j + 1;
                    // continue;
                    if(choix==1) {
                        /*for calcule max*/
                        if (aListDef.get(i).getcArray()[1][0] <= aListDef.get(j).getcArray()[1][1]) {
                            this.cumuleAlreadyVerified(aListDef.get(i).getcArray()[1][0], C, k);
                            k = k + 1;
                            break;
                        } else if (aListDef.get(i).getcArray()[1][0] >= aListDef.get(j).getcArray()[1][1] && j == (aListDef.size() - 1)) {
                            /*max value founded*/
                            mValDef.A[0] = aListDef.get(i).getcArray()[1][0];
                            /*record value of weather, foot*/
                            mValDef.A[1] = aListDef.get(i).getcArray()[0][1];
                            return mValDef;
                        }
                    }else if(choix==2){
                        /*for calcule min*/
                        if (aListDef.get(i).getcArray()[1][0] >= aListDef.get(j).getcArray()[1][1]) {
                            this.cumuleAlreadyVerified(aListDef.get(i).getcArray()[1][0], C, k);
                            k = k + 1;
                            break;
                        } else if (aListDef.get(i).getcArray()[1][0] <= aListDef.get(j).getcArray()[1][1] && j == (aListDef.size() - 1)) {
                            /*min value founded*/
                            mValDef.A[0] = aListDef.get(i).getcArray()[1][0];
                            /*record value of weather, foot*/
                            mValDef.B[0] = aListDef.get(i).getDenomField();
                            return mValDef;
                        }
                    }
                    j = j + 1;
                } while (aListDef.size() > j);
                i = i + 1;
                if(this.isAlready(aListDef.get(i).getcArray()[1][0],C)) {
                    i = i + 1;
                    continue;
                }
            } while (aListDef.size() > i);
            return mValDef;
        } catch (Exception e) {
            System.out.println("Generically exception" + e);
        }
        return null;
    }
    /*pour ne pas iteration for the elements already controlled*/
    public void cumuleAlreadyVerified(int number, int[] C, int i){
        /*number already verified*/
        //C=new int[100];
        C[i]=number;
    }

    public boolean isAlready(int number, int[] C){
        int i=0;
        do{
            if(number==C[i]){
                return true;
            }else{
                return false;
            }
        }while (true);
    }


}
