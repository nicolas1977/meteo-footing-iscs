package calculeNumerical;
import java.util.ArrayList;


public class CalculeFibonacci {
    ArrayList<Integer> arrI = new ArrayList<>();
    private int i;
    private int k;
    int temp = 0;
    int output0=0;
    int output2=1;
    /*solution con calcolo iterativo*/
    public ArrayList<Integer> calculeIteratif(int n){
        try{
            /* noti in inizializzazione input serie */
            arrI.add(0);
            arrI.add(1);

            System.out.println("----------------------------------------------------------");

            while(i<n){
                System.out.println("il "+(i+1)+" simo numero della serie : "+arrI.get(i+1));
                i=i+1;
                // sarebbe lo stesso che A[i+1]=A[i]+A[i-1];
                // o A[i]=A[i-1]+A[i-2];
                // ma ho dovuto usare un ArrayList (unico modo in Java per Array dinamici in dimensione)
                // in Vb.net sarebbe ReDim A(n) (per redimensionale l'array dinamicamente... sono gusti diversi...)
                arrI.add(arrI.get(i)+arrI.get(i-1));
            }
        }catch (Exception e) {
            System.out.println("error found" + e);
        }
        return arrI;
    }

    /*soluzione con calcolo ricursivo*/
    public int calculeRicursif(int output0,int output1, int n){
        if(k==0){
            System.out.println("------------------------------");
        }
        System.out.println("numero della serie : "+output2);
        k=k+1;
        if(k<n){
            output2=output1+output0;
            /*switchando i dummy argument , l'ultimo addendo "output1" diventa il primo "output", e l'ultima somma "output2" (line 47) diventa
            il secondo addendo "output1" (linea 37)*/
            return calculeRicursif(output1,output2,n);
        }else{
            return 0;
        }
    }
}
