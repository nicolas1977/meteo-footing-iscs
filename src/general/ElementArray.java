package general;

public class ElementArray {
    /*values minValue, maxValue, recordsValue, OutputValue*/
    public int cArray[][];
    public String sArray[][];

    public String denomField;

    /*default constructor*/
    public ElementArray(){

    }

    /*constructor specific*/
    public ElementArray(int calculArray[][]){
        cArray=calculArray;
    }

    /*constructor specific*/
    public ElementArray(String field){
        denomField=field;
    }

    /*constructor classic*/
    public ElementArray(int calculArray[][], String field){
        cArray=calculArray;
        denomField=field;
    }

    public int[][] getcArray(){
        return cArray;
    }

    public String getDenomField() {
        return denomField;
    }
}

