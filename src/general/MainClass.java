package general;

import calculeNumerical.Calcule;
import calculeNumerical.CalculeFibonacci;
import calculeNumerical.Permu;
import readers.ReadersFiles;
import general.MultiValue;
import general.ElementArray;

import java.util.RandomAccess;
import java.util.Scanner;


public class MainClass {

    Scanner input;
    public int scelta;


    /*entry point staring program*/
    public static void main(String Args[]) {

        /*the scope is until the end static void main and you need to initialized*/
        MainClass mainClass = null;
        try {

            // instances
            mainClass = new MainClass();
            mainClass.input = new Scanner(System.in);

            /*Scelta del programma dall'user*/
            System.out.println("Scegliere il programma scelto 1 => per Meteo ; 2 => per Foot ; 3 => per Fibonacci ; " +
                    "6 pour Permutation sans repetitions de n numbers");
            mainClass.scelta = mainClass.input.nextInt();

            /*objects instanced class*/
            ReadersFiles readersFiles = new ReadersFiles();
            Calcule cL = new Calcule();
            CalculeFibonacci cLf = new CalculeFibonacci();
            Permu per = new Permu();

            if (mainClass.scelta == 1) {
                //readersFiles.Lecteur("weather.dat");
                System.out.println("-----------------------------------------------------------------");
                System.out.println("The record of WEATHER.dat file with the maximum excurtion is giorno : " +
                        cL.calcule(readersFiles.Lecteur("weather.dat", mainClass.scelta), mainClass.scelta).A[1] + "\n" +
                        "_with the value of excurtion : " + cL.calcule(readersFiles.Lecteur("weather.dat", mainClass.scelta), mainClass.scelta).A[0]);
            } else if (mainClass.scelta == 2) {
                //readersFiles.Lecteur("football.dat");
                System.out.println("-----------------------------------------------------------------");
                System.out.println("The record of FOOTING.dat file records with the minimum difference goals is : \n" +
                        cL.calcule(readersFiles.Lecteur("football.dat", mainClass.scelta), mainClass.scelta).B[0] + "\n" +
                        "_with the value of difference goals AF : " + cL.calcule(readersFiles.Lecteur("football.dat", mainClass.scelta), mainClass.scelta).A[0]);
            } else if (mainClass.scelta == 3) {
                System.out.println("vous avete scelto la serie di fibonacci");
                System.out.println("---------------------------------------");
                System.out.println("indicate se serie a calcolo iterativo (inserire => 4) or ricursivo (inserire => 5):");
                mainClass.scelta = mainClass.input.nextInt();
                if (mainClass.scelta == 4) {
                    /*nell'eventualità dovesse servire in dummy argument d'altri metodi, l'output della funzione calculeRicursif() é un ArrayList*/
                    System.out.println("indicare la quantità di output (numeri di codominio) de la serie Fibonacci da 1 à infinito");
                    cLf.calculeIteratif(mainClass.input.nextInt());
                } else if (mainClass.scelta == 5) {
                    System.out.println("indicare la quantità di output (numeri di codominio) de la serie Fibonacci da 1 à infinito");

                    cLf.calculeRicursif(0, 1, mainClass.input.nextInt());
                }
            } else if (mainClass.scelta == 6) {
                System.out.println("indicare la quantità de number to permutation sans repetitions");
                int n = mainClass.input.nextInt();

                per.listePermutations(per, n);
            }
            // TODO conditions after

        } catch (Exception e) {
            System.out.println("error found" + e);
        } finally {
            if (mainClass.scelta == 1) {
                System.out.println("-----------------");
                System.out.println("exit program Meteo");
            }
            if (mainClass.scelta == 2) {
                System.out.println("-----------------");
                System.out.println("Exit program Foot");
            } else if (mainClass.scelta == 4 || mainClass.scelta == 5) {
                System.out.println("-----------------");
                System.out.println("Exit program Fibonacci");
            }
        }
        if (mainClass.scelta == 6) {
            System.out.println("-----------------");
            System.out.println("Exit program permutation sans repetition");
        }

    }
}
