package general;

/*This class is used only from external and functionnality value passing in dummy argument
so is impossible in Java to passing a multiple return value as Python 3 with a builting-in Array
return value*/

public class MultiValue{
    public int A[];
    public String B[];

    /*default constructor*/
    public MultiValue(){
        //
    }

    /*constructor specific*/
    public MultiValue(int intA[]){
        A=intA;
    }

    /*constructor specific*/
    public MultiValue(String strB[]){
        B=strB;
    }

    /*constructor classic*/
    public MultiValue(int intA[], String strB[]){
        A=intA;
        B=strB;
    }

}
